<?php get_header(); ?>
<div class="content">

     <div class="page-header">
        <h1 class="page-header__title"><?php the_title(); ?></h1>
        <h3 class="page-header__subtitle">We would love to hear from you!</h3>
    </div>

    <div class="contact-box main-wrapper">
        <!-- Contact Form, Map, Details -->
        <div class="contact-box__inner">
            <h1>Get in touch with us</h1>
            <div class="contact-box__details">
                    <div class="contact-item"><i class="fa fa-envelope-open fa-2x"></i><span>Email</span></div>
                    <div class="contact-item"><i class="fa fa-facebook fa-2x"></i><span>Facebook</span></div>
                    <div class="contact-item"><i class="fa fa-twitter fa-2x"></i><span>Twitter</span></div>
                    <div class="contact-item"><i class="fa fa-linkedin fa-2x"></i><span>LinkedIn</span></div>
            </div>
            <div class="contact-box__bottom">
                <div class="contact-box__bottom__form">
                    <?php echo do_shortcode('[wpforms id="465"]'); // dev: 51 ?>
                </div>
                <div class="contact-box__bottom__map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d99370.5499069078!2d-77.08461555384616!3d38.89357552995622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89b7c6de5af6e45b%3A0xc2524522d4885d2a!2sWashington%2C+DC!5e0!3m2!1sen!2sus!4v1540433351042" width="400" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

</div>
<?php get_footer(); ?>
