<?php 
// Classes and Structure for the theme

/**
 * This class is used to build the structure of my Sections with the ACF plugin. It will check for new blocks/sections and add them to the ACF array to display the layouts on each page. 
 */
class quseSections
{
    // Array to store each of the blocks/sections that my theme has
    public $block_array = array();

    // Array for the active blocks/sections the user has selected
    public $checked = array();

    public function getBlocks()
    {

        // Get the directory listing that we need to look for the files
        $handle = opendir(dirname(realpath(__FILE__)).'/blocks/');

        // While in that directory, search for files and add them to the array, minus the exceptions
        while($file = readdir($handle)){
            if($file !== '.' && $file !== '..' && $file !== 'style'){
                $block_array[$file] = ucfirst($file);
            }
        }

        return $block_array;

    }

    /**
    * Now that we have the blocks collect in the theme files, lets collect a list of blocks the user wants to use.
    * This function will set the active blocks that the admin choose to use
    */
    public function setActive_Blocks( $sections )
    {
        // Check the values being passed in; if not empty go thru each create a new array.
        if ( !empty( $sections ) ) :
            foreach ($sections as $section) :
                $this->checked[$section] = $section;
            endforeach;

            return $this->checked;
        else: 
             // echo "No Active Layouts";
        endif;
    }

    /**
    * Ok, now that we have the blocks that the admin wants to use, lets build the fields that will be used in the admin for each page.
    */
    public function getActive_Sections()
    {
        $output = array();

        // foreach active section, get the path to its fields array build a new array for the ACF fiexible content layouts
        foreach ($this->checked as $active) :

            include dirname(realpath(__FILE__)).'/blocks/' . $active . '/fields.php';

            $output[$active] = $fields[$active];

        endforeach;

        return $output;
    }

}

/**
 * This class will put together each of the components (blocks) that has been activated. This class will build the scss, js, and acf fields and package them up for the site.
 */
class quseLayouts extends quseSections
{
    public $fields = array();
    public $active = array();

    // This function runs everytime the class is called and it will get the layout objects from acf and build an array of active fields to use
    public function __construct()
    {
        if ( function_exists( 'get_field') ): 

            $acf_fields = get_field_objects();

            if ( !empty($acf_fields['quse_sections']['value']) ) :
            
                foreach ($acf_fields['quse_sections']['value'] as $field):
                    
                    $active_fields = $field['acf_fc_layout'];

                    // Build an array with all of the fields tied to the block name
                    $this->fields[$active_fields] = $field;

                    // Build an array of active blocks
                    $this->active[$active_fields] = $active_fields;

                endforeach;
                
                return $this->active;

            endif;

        endif;
    }

    // This will build the structure of each of my components, enque the js and scss and push to the template file
    public function getLayout( $block )
    {
        // This is the function to enqueue the js files
        if ( file_exists( get_theme_file_path("/blocks/$block/js") ) ) {

            $name = $block . '-main-js';

            wp_register_script($name, get_template_directory_uri() . '/blocks/' . $block . '/js/main.js', array(), '1.0');
            wp_enqueue_script($name); // Enqueue it!

        }

        if ( file_exists( get_theme_file_path("/blocks/style/$block.scss") ) ) {

            $name = $block . '-main-scss';

            wp_register_style($name, get_template_directory_uri() . '/blocks/style/' . $block . '.scss', array(), '1.0', 'all');
            wp_enqueue_style($name); // Enqueue it!

        }
        
    }

}

