<?php get_header(); ?>
<div class="content">
    
    <?php

        if (have_posts()): while (have_posts()) : the_post(); ?>

        <div class="main-wrapper">

            <?php 
            // New Block style is passing everything through the wordpress content.
            the_content(); 
        
        endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'qusedev' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>
            
        </div>

        <?php

        if ( ! is_user_logged_in() ) { ?>
            <div class="cont">
                <div class="form sign-in">
                    <h1>Sign In</h1>
                    <?php wp_login_form(); ?>
                </div>
                <div class="sub-cont">
                    <div class="img">
                        <div class="img__text m--up">
                            <h2>New here?</h2>
                            <p>Sign up and get the latest updates and try out my products!</p>
                        </div>
                        <div class="img__text m--in">
                            <h2>One of us?</h2>
                            <p>If you already have an account, just sign in here.</p>
                        </div>
                        <div class="img__btn">
                            <span class="m--up">Sign Up</span>
                            <span class="m--in">Sign In</span>
                        </div>
                    </div>
                    <div class="message"></div>
                    <div class="form sign-up">
                        <form method="post" id="Sign-up">
                            <h1>Join the Movement</h1>
                            <label>
                                <span>First Name</span>
                                <input type="text" class="form-control" id="first_name" name="first_name" data-parsley-length="[4, 20]" required />
                            </label>
                            <label>
                                <span>Last Name</span>
                                <input type="text" class="form-control" id="last_name" name="last_name" data-parsley-length="[4, 30]" required />
                            </label>
                            <label>
                                <span>Email</span>
                                <input type="email" class="form-control" id="email" name="email" data-parsley-type="email" data-parsley-trigger="keyup" max-length="40" required />
                            </label>
                            <input class="submit button button--primary" type="submit" name="register" value="Sign Up" />
                        </form>
                    </div>
                </div>
            </div>
    <?php } ?>

</div><!-- End content -->
<?php get_footer(); ?>