<?php get_header(); ?>
<div class="content">

    <div class="page-header">
        <h1 class="page-header__title"><?php the_title(); ?></h1>
        
    </div>

    <div class="main-wrapper single">

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php the_content(); // Dynamic Content ?>

            <p>
                <?php _e( 'Categorised in: ', 'quse' ); the_category(', '); // Separated by commas ?><br />
                <span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
            </p>
            
		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'qusedev' ); ?></h1>

		</article>
		<!-- /article -->

    <?php endif; ?>
</div>

</div>

<?php get_footer(); ?>
