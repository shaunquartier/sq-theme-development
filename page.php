<?php get_header(); ?>
<main role="main">

        <?php if (have_posts()): while (have_posts()) : the_post();

            if ( get_page_template_slug( $post->ID ) == 'resources-template.php' ) :
                quse_admin_settings();
            endif;
        ?>

        <div class="main-wrapper">

            <?php 
            // New Block style is passing everything through the wordpress content.
            the_content(); 

            // Old Class to call my layouts (classic editor)
            //$layouts = new quseLayouts;
            //$layouts->getLayout();
            ?>
            
        </div>

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'qusedev' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

	</main>

<?php get_footer(); ?>
