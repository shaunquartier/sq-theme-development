<?php get_header(); ?>
<div class="content">

    <div class="page-header">
        <h1 class="page-header__title"><?php the_title(); ?></h1>
    </div>

    <div class="main-wrapper pad-top">
    <div class="box__grid">
            <div class="box__grid__column">
                <div class="box box--small">
                    <div class="box__inner" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/default-feature.jpg')">
                        <div class="box__inner__content">
                            <div class="box__inner__content__title">Custom Wordpress Theme</div>
                            <div class="box__inner__content__description">Setup and maintain your website with a few clicks.</div>
                        </div>
                    </div>
                </div>
                <div class="box box--small">
                    <div class="box__inner" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/blocks.jpg')">
                        <div class="box__inner__content">
                            <div class="box__inner__content__title">Gutenberg Blocks</div>
                            <div class="box__inner__content__description">Easily create custom content for your site.</div>
                        </div>
                    </div>
                </div>
                <div class="box box--small">
                    <div class="box__inner" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/coming-soon-feature.jpg')">
                        <div class="box__inner__content">
                            <div class="box__inner__content__title">Admin Project Tacker</div>
                            <div class="box__inner__content__description">Track every piece of your website and workflow.</div>
                        </div>
                    </div>
                </div>
                <div class="box box--small">
                    <div class="box__inner" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/coming-soon-feature.jpg')">
                        <div class="box__inner__content">
                            <div class="box__inner__content__title">Issue Tracker</div>
                            <div class="box__inner__content__description">Add issues/bugs/feaures needed for your website.</div>
                        </div>
                    </div>
                </div>
                <div class="box box--small">
                    <div class="box__inner" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/resources.jpg')">
                        <div class="box__inner__content">
                            <div class="box__inner__content__title">Document Manager</div>
                            <div class="box__inner__content__description">Add/Edit/Delete documents tied to your project.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php get_footer(); ?>