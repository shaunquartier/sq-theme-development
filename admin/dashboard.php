<?php
function quse_dashboard_content() {

    $admin_options = new quseOptions;
    $show_options = $admin_options->getOptions();
    ?>
    <div class="project_info">
        <h1>Welcome <?php echo $show_options['name']; ?> to the Quse Development Dashboard!</h1>
        <p>This dashboard will you show the current theme settings for your website. It will also provide quick access to important project information, billing details, and links to helpful guides.</p>
        <div class="project_info__help">
            <div class="project_info__help__links project_info__box">
                <h3>Admin Website Links</h3>
                <a class="action-link" href="#"><span class="dashicons dashicons-admin-page"></span> Add/Edit Pages</a>
                <a class="action-link" href="#"><span class="dashicons dashicons-menu-alt"></span> Update Menu</a>
                <a class="action-link" href="#"><span class="dashicons dashicons-admin-generic"></span> Add/Update Theme Settings</a>
            </div>
            <div class="project_info__help__billing project_info__box">
                <h3>Project Information</h3>
                <a class="action-link" href="#"><span class="dashicons dashicons-welcome-widgets-menus"></span> Website/Domain Renewal</a>
                <a class="action-link" href="#"><span class="dashicons dashicons-media-code"></span> Log a bug</a>
                <a class="action-link" href="#"><span class="dashicons dashicons-email-alt"></span> Contact Admin Team</a>
            </div>
        </div>
        <h3>Site Layout & Information</h3>
        <p>The layout below will show your global theme settings. An administrator can configure these options using the menu on the left hand side. Click on 'Quse Settings' to access these options.</p>
    </div>
    <div class="admin">
    <div class="admin_header box" style="background-color: <?php echo $show_options['bg_header_color']; ?>">
        <img src="<?php echo $show_options['logo']['url']; ?>" />
        <h3>Header Layout</h3>
    </div>
    <div class="admin_body">
        <h4>Google Analytics ID: <?php echo $show_options['analytics']; ?></h4>
        <h4>Active Blocks:</h4>
    </div>
    <div class="admin_footer box" style="background-color: <?php echo $show_options['bg_footer_color']; ?>">
        <h4>Copyright: <?php echo $show_options['copyright']; ?></h4>
        <h3>Footer Layout</h3>
    </div>
    </div>
    
    <?php // echo '<pre>'; print_r($show_options); echo '</pre>'; ?>
    
<?php }