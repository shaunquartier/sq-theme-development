<?php // echo "<pre>" ; print_r($values); echo "</pre>"; ?>

<div class="card">

    <div class="main_wrapper features card__wrapper">

        <?php foreach ( $values['card_items'] as $item ) : ?>

            <div class="card__item card__item--<?php echo $values['card_columns_to_show']; ?>">

                <div class="card__item__wrapper link">

                    <div class="card__item__image card-top">

                        <img src="<?php echo wp_get_attachment_url( $item['card_image'] ); ?>" />

                    </div>

                    <div class="card__item__inner card-body">

                        <h2 class="card__item__title card-title">

                            <?php echo $item['card_title']; ?>

                        </h2>

                        <div class="card__item__content">

                            <?php echo $item['card_content']; ?>

                        </div>

                        <div class="card__item__link">

                            <a class="button button--primary" href="<?php echo $item['card_url']; ?>"><?php echo $item['card_link_label']; ?></a>

                        </div>

                    </div>

                </div>

            </div>

        <?php endforeach; ?>

    </div>

</div>
