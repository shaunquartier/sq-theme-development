<?php
acf_add_local_field_group(array(
	'key' => 'card_block_fields',
	'title' => 'Card',
	'fields' => array(

		array (
			'key' => 'card_items',
			'label' => 'Cards',
			'name' => 'card_items',
			'type' => 'repeater',
			'layout' => 'block',
			'button_label' => 'Add Card',
			'sub_fields' => array (

                // Title Tab
                array(
                    'key' => 'card_title--tab',
                    'label' => 'Title',
                    'name' => '',
                    'type' => 'tab',
                ),

                array (
                    'key' => 'card_title',
                    'label' => 'Title',
                    'name' => 'card_title',
                    'type' => 'text',
                    'instructions' => 'Enter a title for your card',
                ),

                // Content Tab
                array(
                    'key' => 'card_content--tab',
                    'label' => 'Content',
                    'name' => '',
                    'type' => 'tab',
                ),

                array (
                    'key' => 'card_content',
                    'label' => 'Content',
                    'name' => 'card_content',
                    'type' => 'wysiwyg',
                ),

                // Image Tab
                array(
                    'key' => 'card_image--tab',
                    'label' => 'Image',
                    'name' => '',
                    'type' => 'tab',
                ),

                array (
                    'key' => 'card_image',
                    'label' => 'card Image',
                    'name' => 'card_image',
                    'type' => 'image',
                    'return_format' => 'array',
                    'preview_size' => 'thumbnail',
                    'library' => 'all',
                ),
                
                // Link Tab
                array(
                    'key' => 'card_link--tab',
                    'label' => 'Link',
                    'name' => '',
                    'type' => 'tab',
                ),

                array (
                    'key' => 'card_link_label',
                    'label' => 'Link Label',
                    'name' => 'card_link_label',
                    'type' => 'text',
                    'instructions' => 'Enter a label for your link',
                ),
                
                array (
                    'key' => 'card_link',
                    'label' => 'Page Link or URL?',
                    'name' => 'page_or_url',
                    'type' => 'select',
                    'choices' => array (
                        'url' => 'URL',
                        'page' => 'Page Link',
                    ),
                    'wrapper' => array (
                        'width' => '50%',
                    ),
                ),
                array (
                    'key' => 'card_page_link',
                    'label' => 'Page',
                    'name' => 'card_url',
                    'type' => 'page_link',
                    'conditional_logic' => array (
                        array (
                            array (
                                'field' => 'card_link',
                                'operator' => '==',
                                'value' => 'page',
                            ),
                        ),
                    ),
                    'wrapper' => array (
                        'width' => '50%',
                    ),
                ),
                array (
                    'key' => 'card_url_link',
                    'label' => 'URL',
                    'name' => 'card_url',
                    'type' => 'url',
                    'conditional_logic' => array (
                        array (
                            array (
                                'field' => 'card_link',
                                'operator' => '==',
                                'value' => 'url',
                            ),
                        ),
                    ),
                    'wrapper' => array (
                        'width' => '50%',
                    ),
                ),
                
			),
        ),
        array(
            'key' => 'card_options--accordion',
            'label' => 'Advanced Options',
            'name' => '',
            'type' => 'accordion',
        ),
        array (
            'key' => 'card_columns_to_show',
            'label' => 'Columns',
            'name' => 'card_columns_to_show',
            'type' => 'number',
            'instructions' => 'Enter the number of columns you want the cards split into on desktop',
            'default_value' => 4,
            'max' => 4,
            'step' => 1,
        ),
    ),
    'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/card',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
    )
);
