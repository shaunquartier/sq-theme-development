<?php
acf_add_local_field_group(array(
	'key' => 'carousel_block_fields',
	'title' => 'Carousel',
	'fields' => array(

		array (
			'key' => 'carousel_slides',
			'label' => 'Slide(s)',
			'name' => 'carousel_slides',
			'type' => 'repeater',
			'layout' => 'block',
			'button_label' => 'Add Slide',
			'sub_fields' => array (

                // Image Tab
                array(
					'key' => 'slide_image--tab',
					'label' => 'Image',
					'name' => '',
					'type' => 'tab',
			    ),

				array (
					'key' => 'slide_image',
					'label' => 'Slide Image',
					'name' => 'image',
					'type' => 'image',
					'return_format' => 'array',
					'preview_size' => 'thumbnail',
					'library' => 'all',
                ),

                // Title Tab
                array(
                    'key' => 'slide_title--tab',
                    'label' => 'Title',
                    'name' => '',
                    'type' => 'tab',
                ),

                array (
                    'key' => 'slide_title',
                    'label' => 'Title',
                    'name' => 'slide_title',
                    'type' => 'text',
                    'instructions' => 'Enter a title for your slide',
                ),

                // Content Tab
                array(
                    'key' => 'slide_content--tab',
                    'label' => 'Content',
                    'name' => '',
                    'type' => 'tab',
                ),

                array (
                    'key' => 'slide_content',
                    'label' => 'Content',
                    'name' => 'slide_content',
                    'type' => 'text',
                ),
                
			),
        ),
    
    ),
    'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/carousel',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
    )
);
