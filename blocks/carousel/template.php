<?php // echo "<pre>" ; print_r($values); echo "</pre>"; ?>

<div id="quse-carousel" class="carousel slide carousel-fade" data-ride="carousel">

    <div class="carousel__wrapper carousel-inner">

        <?php foreach ( $values['carousel_slides'] as $slide ) : ?>

            <div class="carousel__item carousel-item">

                <img class="carousel__item__image d-block w-100" src="<?php echo wp_get_attachment_url( $slide['slide_image'] ); ?>" />

                <div class="carousel-container">

                    <?php if ( !empty( $slide['slide_title'] || $slide['slide_content'] )) {?>
                    
                        <div class="carousel-caption text-center">

                            <h1><?php echo $slide['slide_title']; ?></h1>

                            <div class="carousel__item__text d-none d-md-block">

                                <?php echo $slide['slide_content']; ?>

                            </div>

                        </div>
                    
                    <?php } ?>

                </div>

            </div>
            
        <?php endforeach; ?>

    </div>

    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>

    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>

</div>

<script>
  $(document).ready(function () {
    $('.carousel').find('.carousel__item').first().addClass('active');
  });
</script>