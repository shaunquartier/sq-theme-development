$(document).ready(function(){

	// target the handle of every accordion item on click of the handle
	$('.accordion__item__tab').on('click', function(){

		//find the parent accordion item
		var $parent = $(this).closest('.accordion__item');

		// toggle ddc_accordion__item--open class
		$parent.toggleClass('accordion__item--open');

	});

});