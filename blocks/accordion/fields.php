<?php
acf_add_local_field_group(array(
	'key' => 'accordion_block_fields',
	'title' => 'Accordion',
	'fields' => array(

		array (
			'key' => 'accordion_items',
			'label' => 'Items',
			'name' => 'items',
			'type' => 'repeater',
			'layout' => 'block',
			'button_label' => 'Add Accordion Item',
			'sub_fields' => array (

                // Title Tab
                array(
                    'key' => 'accordion_title--tab',
                    'label' => 'Title',
                    'name' => '',
                    'type' => 'tab',
                ),

                array (
                    'key' => 'item_title',
                    'label' => 'Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Enter an accordion title',
                    'required' => 1,
                ),

                // Content Tab
                array(
                    'key' => 'accordion__tab__content',
                    'label' => 'Content',
                    'name' => '',
                    'type' => 'tab',
                ),

                array (
                    'key' => 'item_content',
                    'label' => 'Content',
                    'name' => 'content',
                    'type' => 'wysiwyg',
                ),
			),
        ),
        array(
            'key' => 'accordion_options--accordion',
            'label' => 'Advanced Options',
            'name' => '',
            'type' => 'accordion',
        ),
        array (
            'key' => 'item_tab_color',
            'label' => 'Tab Color',
            'name' => 'tab_color',
            'type' => 'color_picker',
            'instructions' => 'This is the background color of the tab',
            'wrapper' => array (
                'width' => '50%',
            ),
        ),

        array (
            'key' => 'item_tab_color_active',
            'label' => 'Tab Color Active',
            'name' => 'tab_color__active',
            'type' => 'color_picker',
            'instructions' => 'This is the background color of the tab when the item is active',
            'wrapper' => array (
                'width' => '50%',
            ),
        ),

        array (
            'key' => 'item_title_color',
            'label' => 'Title Color',
            'name' => 'title_color',
            'type' => 'color_picker',
            'instructions' => 'This is the text color of the title',
            'wrapper' => array (
                'width' => '50%',
            ),
        ),

        array (
            'key' => 'item_title_active_color',
            'label' => 'Title Color Active',
            'name' => 'title_color__active',
            'type' => 'color_picker',
            'instructions' => 'This is the text color of the title when the tab is active.',
            'wrapper' => array (
                'width' => '50%',
            ),
        ),
    ),
    'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/accordion',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
    )
);

