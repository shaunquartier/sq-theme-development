<?php // echo "<pre>" ; print_r($values); echo "</pre>"; ?>
<div class="accordion">

    <div class="main-wrapper accordion__wrapper">

        <?php foreach ( $values['accordion_items'] as $item ) : ?>
        
            <div class="accordion__item">

                <div class="accordion__item__tab" style="background:<?php echo $values['item_tab_color']; ?>; color:<?php echo $values['item_title_color']; ?>">

                    <div class="accordion__item__tab__title">

                        <?php echo $item['item_title']; ?>

                    </div>

                    <div class="accordion__item__tab__icon">

                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                    
                    </div>
                
                </div>

                <div class="accordion__item__content">

                    <div class="accordion__item__content__inner">

                        <?php echo $item['item_content']; ?>
                    
                    </div>
                
                </div>
            
            </div>

        <?php endforeach; ?>

    </div>

</div>

<style type="text/css">

    .accordion__item--open .accordion__item__tab {
        color: <?php echo $values['title_color__active']; ?>!important;
        background: <?php echo $values['tab_color__active']; ?>!important;
    }

</style>