<?php
acf_add_local_field_group(array(
	'key' => 'text_block_fields',
	'title' => 'Text',
	'fields' => array(

		array(
			'key' => 'text_content',
			'label' => 'Content',
			'name' => 'text_content',
			'type' => 'wysiwyg',
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 1,
			'delay' => 1,
        ),
        
	),
	'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/text',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));