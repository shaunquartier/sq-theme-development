<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php bloginfo('description'); ?>">

    <?php wp_head(); ?>

</head>
<?php
$admin_options = new quseOptions;
$show_options = $admin_options->getOptions();
// Global Site Tag (gtag.js) - Google Analytics
$analytics = $show_options['analytics'];

// WP Nav to work with Bootstrap
$nav_args = array (
    'menu'              => $show_options['header_menu'],
    'container'         => 'div',
    'container_class'   => 'collapse navbar-collapse nav--main',
    'container_id'      => 'navbarNav',
    'menu_class'        => 'navbar-nav',
);

/*
if ( ! empty( $analytics ) ) { ?>
    
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $gaCode; ?>"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments)};
    gtag('js', new Date());

    gtag('config', '<?php echo $analytics; ?>');
    </script>

<?php } 
*/
?>

<body <?php body_class(); ?>
    
<!-- wrapper -->
<?php if ( is_home() ) {

    $page_for_posts = get_option( 'page_for_posts' ); ?>

    <div class="wrapper posts" style="background-image: url('<?php echo get_the_post_thumbnail_url( $page_for_posts ); ?>');">

<?php } else { ?>

    <div class="wrapper" <?php if (has_post_thumbnail( $post->ID ) ): ?>style="background-image: url('<?php  the_post_thumbnail_url(); ?>');" <?php endif; ?>>

<?php }; ?>

<!-- header -->
<header class="header clear" role="banner">

    <nav class="navbar navbar-expand-lg fixed-top navbar-dark nav header_nav" role="navigation" style="background: <?php echo $show_options['bg_header_color']; ?>">

        <a class="navbar-brand" href="<?php echo home_url(); ?>">
            <img src="<?php echo $show_options['logo']['url']; ?>" alt="Logo" class="logo-img">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <?php wp_nav_menu( $nav_args ); 

            echo "<ul class='nav social-media'>";

                if ( $show_options['social_placement'] == 'header' ) {

                    $social_media = $show_options['social_media'];

                    // Foreach social media item displace the link
                    foreach ( $social_media as $sm) {

                        echo "<li class='nav-item icon'>";
                            echo "<a style='color: " . $sm['icon_color'] . "' class='nav-link' target='_blank' href='" . $sm['url'] . "'><i class='fa " . $sm['fa_icon'] . "'></i></a>";
                        echo "</li>";
                    }

                }

                /*
                if ( is_user_logged_in() ) {

                    if ( is_plugin_active( 'quse-resources/quse-resources.php' ) ) :
                        echo "<li class='nav-item icon'><a title='View Resources' class='nav-link' href='/manager'><i class='fa fa-briefcase'></i></a></li>";
                    endif;
                    echo "<li class='nav-item'><a data-toggle='collapse' href='#collapseExample' role='button' aria-expanded='false' aria-controls='collapseExample'>View Portal</a></li>";
                    echo "<li class='nav-item icon'><a class='nav-link' title='Logout' href='" . wp_logout_url( home_url() ) . "'><i class='fa fa-sign-out'></i></a></li>";
                }
                */
                
            echo "</ul>"; ?>

    </nav>

</header>
