</div> <!-- /wrapper -->

<?php 
$admin_options = new quseOptions;
$show_options = $admin_options->getOptions();
?>

<!-- footer -->
<footer class="footer" role="contentinfo" style="background: <?php echo $show_options['bg_color']; ?>">

    <p class="copyright">
        &copy; <?php echo date('Y') . ' ' .  $show_options['copyright']; ?>
    </p>

</footer>

<?php wp_footer(); ?>

</body>
</html>
