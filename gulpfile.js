// Gulp Nodes   ``` npm install --no-bin-links ```
var gulp = require( 'gulp' ),
	plumber = require( 'gulp-plumber' ),
	sass = require( 'gulp-sass' ),
	autoprefixer = require('gulp-autoprefixer'),
	minifyCSS = require('gulp-clean-css'),
	watch = require( 'gulp-watch' ),
	globImporter = require('sass-glob-importer'),
	uglify = require( 'gulp-uglify' ),
	rename = require( 'gulp-rename' ),
	concat = require('gulp-concat');

// Error Handling
var onError = function( err ) {
	console.log( 'An error occurred:', err.message );
	this.emit( 'end' );
};

// sass
gulp.task('sass', function () {
  return gulp.src( './css/style.scss',  { sourcemap: true } )
  	.pipe(sass({ importer: globImporter() }))
    .pipe ( sass().on('error', sass.logError ) )
    .pipe( autoprefixer( 'last 4 version' ) )
    .pipe( minifyCSS( { keepBreaks: false,  } ) )
    .pipe( gulp.dest('./') );
});

// js
/*
gulp.task('js', function() {
	return gulp.src( ['./js/src/*.js' ] )
		.pipe( concat( './js/site.js' ) )
		.pipe( gulp.dest( './' ) )
		.pipe( rename( { suffix: '.min' } ) )
		.pipe( uglify() )
		.pipe( gulp.dest('./') );
});
*/

// watch
gulp.task( 'watch', function() {

	// Watch all .scss files
	gulp.watch( ['./css/*.scss', './**/**/*.scss', './css/*/src/*.scss', './blocks/*/style/*.scss'], [ 'sass' ] );
	//gulp.watch( ['./js/src/*.js' ], [ 'js' ] );

});

// Default task -- runs scss and watch functions
gulp.task( 'default', ['js', 'sass', 'watch'] );
