<?php 

if ( isset( $_POST['register'] ) ) {

    $user = $_POST['first_name'] . ' ' . $_POST['last_name'];
    $uname = $_POST['first_name'] . $_POST['last_name'];
    $pass = wp_generate_password( $length = 12, $special_chars = true, $extra_special_chars = false );
    $email = $_POST['email'];
    $website = "https://qusedevelopment.com";
    $userdata = array(
        'user_login'  =>  $uname,
        'first_name' => $_POST['first_name'],
        'last_name' => $_POST['last_name'],
        'user_email' => $_POST['email'],
        'user_pass'   =>  $pass  // When creating a new user, `user_pass` is expected.
    );

    if ( !username_exists( $user )  && !email_exists( $email ) ) {
        $user_id = wp_insert_user( $userdata ) ;
        add_user_meta( $user_id, 'project', $_POST['project'] );
        add_user_meta( $user_id, 'site_url', $website );
        $user = new WP_User( $user_id );
        $user->set_role( 'subscriber' );

        //On success
        if ( ! is_wp_error( $user_id ) ) {
            //Redirect
            wp_redirect( '/thank-you' );
            exit;
        } else {
            // Set a 500 (internal server error) response code.
            http_response_code(500);
            echo "Oops! Something went wrong and we couldn't register you.";
        }
    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        echo "There was a problem with your submission, user already exists.";
    }

} // End if post is submitted