<?php
/* define the admin settings */
if( function_exists('acf_add_options_page') ) {

    // Include theme, header, and footer settings and build array
    include 'theme/theme-custom-fields.php';

	acf_add_options_page (
        array (
            'page_title' 	=> 'Quse Settings',
            'menu_title'	=> 'Quse Settings',
            'menu_slug' 	=> 'general-settings',
            'capability'	=> 'edit_posts',
            'redirect'		=> false
        )
    );

    $site_options = array(
        'analytics' => get_field('site_analytics', 'option'),
        'social_placement' => get_field('social_placement', 'option'),
        'social_media' => get_field('site_social', 'option'),
        'logo' => get_field('header_logo', 'option'),
        'bg_header_color' => get_field('header_bg_color', 'options'),
        'copyright' => get_field('footer_copyright', 'option'),
        'bg_footer_color' => get_field('footer_bg_color', 'options')
    );

    // If there are active sections checked, then include the fields for acf
    $newObj = new quseSections;
    $modules = $newObj->getBlocks();
    
    if ( !empty( $modules ) ) {

        $active_blocks = $newObj->setActive_Blocks($site_options['sections']);

        /* Testing for ACF 5.8 and using Gutenberg blocks

        foreach ($modules as $module ) {

            acf_register_block( array(
                'name'				=> $module,
                'title'				=> __('Quse - ' . ucfirst($module)),
                'description'		=> __('A custom ' . $module . ' block.'),
                'render_callback'	=> 'quse_block_render_callback',
                'category'			=> 'common',
                'icon'				=> 'schedule',
                'mode'              => 'preview',
                'keywords'			=> array( $module, 'layout', 'Quse' ),
            ));

            // Setting the block name to lowercase so that there is no confusion with the include path
            $name = strtolower($module);
            
            if ( file_exists( get_theme_file_path("/blocks/$name/fields.php") ) ) {
                include get_template_directory() . '/blocks/' . $name . '/fields.php';
            } else {
                echo "Sorry, no block data to load.";
            }

        }

        function quse_block_render_callback( $block ) {
    
            $slug = str_replace('acf/', '', $block['name']);

            if ( !empty($block['data'] ) ) {

                $layouts = new quseLayouts;
                $layouts->getLayout($slug);

                $values = array();

                 // The data get stored inside this array so I am cleaning it up for my template.
                foreach ( $block['data'] as $data => $pair ) {

                    $values[$data] = $pair;

                }

            }

            // If the template exists for this block, lets call it.
            if( file_exists( get_theme_file_path("/blocks/$slug/template.php") ) ) {
                
                include( get_theme_file_path("/blocks/$slug/template.php") );

            }

        }

        End of Testing */

    };

    // Store the array in a session
    session_start();
    $_SESSION['site-options'] = $site_options;

} //End If: ACF options exist