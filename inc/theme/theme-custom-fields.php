<?php
// $obj = new quseSections;

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_theme',
	'title' => 'Theme Settings',
	'fields' => array (

        // Theme Settings Tab
        array(
            'key' => 'quse_theme_settings',
            'label' => 'Theme Options',
            'name' => '',
            'type' => 'tab',
        ),

        array(
            'key' => 'site_analytics',
            'label' => 'Google Analytics Property Code',
            'name' => 'analytics',
            'type' => 'text',
            'instructions' => 'Input the Google Analytics property ID',
        ),
        
        array(
			'key' => 'site_link_color',
			'label' => 'Link Color',
			'name' => 'link_color',
			'type' => 'color_picker',
			'instructions' => 'Choose the link color for your website',
            'wrapper' => array(
				'width' => '50',
			),
        ),

        array(
			'key' => 'site_layouts',
			'label' => 'Active Blocks',
			'name' => 'layouts',
			'type' => 'checkbox',
			'instructions' => '',
			'choices' => $obj->getBlocks(),
			'layout' => 'vertical',
			'return_format' => 'value',
        ),
        
        // Header Settings Tab
        array(
            'key' => 'quse_header_settings',
            'label' => 'Header',
            'name' => '',
            'type' => 'tab',
        ),

        array(
            'key' => 'header_logo',
            'label' => 'Logo',
            'name' => 'logo',
            'type' => 'image',
            'instructions' => 'Upload an image to use for the logo of your website',
            'return_format' => 'array',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'wrapper' => array(
                'width' => '50',
            ),
        ),

        array(
            'key' => 'header_background_color',
            'label' => 'Background Color',
            'name' => 'header_bg_color',
            'type' => 'color_picker',
            'instructions' => 'Choose the color for your background',
            'wrapper' => array(
                'width' => '50',
            ),
        ),

        // Footer Settings Tab
        array(
            'key' => 'quse_footer_settings',
            'label' => 'Footer',
            'name' => '',
            'type' => 'tab',
        ),

        array(
            'key' => 'footer_background_color',
            'label' => 'Background Color',
            'name' => 'footer_bg_color',
            'type' => 'color_picker',
            'instructions' => 'Choose the color for your background',
            'wrapper' => array(
                'width' => '50',
            ),
        ),
        array(
            'key' => 'footer_copyright',
            'label' => 'Copyright Text',
            'name' => 'copyright',
            'type' => 'text',
            'instructions' => 'Input the text you want to display for the Copyright',
            'wrapper' => array(
                'width' => '50',
            ),
        ),

        // Social Media Tab
        array(
            'key' => 'quse_social_settings',
            'label' => 'Social Media',
            'name' => '',
            'type' => 'tab',
        ),

        array (
            'key' => 'social_placement',
            'label' => 'Icons Placement',
            'name' => 'social_placement',
            'type' => 'select',
            'choices' => array (
                'none' => 'Do Not Show',
                'header' => 'Header',
                'footer' => 'Footer',
            ),
        ),

        array (
            'key' => 'site_social',
            'label' => 'Social Media Icons',
            'name' => 'social',
            'type' => 'repeater',
            'instructions' => '',
            'layout' => 'block',
            'button_label' => 'Add Icon',
            'sub_fields' => array (

                array (
                    'key' => 'social_pou',
                    'label' => 'Page or URL?',
                    'name' => 'social_post_or_url',
                    'type' => 'select',
                    'wrapper' => array(
                        'width' => '50',
                    ),
                    'choices' => array (
                        'URL' => 'Social URL',
                        'Page Link' => 'Social Page Link',
                    ),
                ),

                array (
                    'key' => 'social_page_link',
                    'label' => 'Page',
                    'name' => 'url',
                    'type' => 'page_link',
                    'wrapper' => array(
                        'width' => '50',
                    ),
                    'conditional_logic' => array (
                        array (
                            array (
                                'field' => 'social_pou',
                                'operator' => '==',
                                'value' => 'Page Link',
                            ),
                        ),
                    ),
                ),

                array (
                    'key' => 'social_url',
                    'label' => 'URL',
                    'name' => 'url',
                    'type' => 'url',
                    'wrapper' => array(
                        'width' => '50',
                    ),
                    'conditional_logic' => array (
                        array (
                            array (
                                'field' => 'social_pou',
                                'operator' => '==',
                                'value' => 'URL',
                            ),
                        ),
                    ),
                ),

                array (
                    'key' => 'social_iofa',
                    'label' => 'Image or Font Awesome?',
                    'name' => 'img_or_fa',
                    'type' => 'select',
                    'wrapper' => array(
                        'width' => '50',
                    ),
                    'choices' => array (
                        'Image' => 'Image',
                        'Font Awesome' => 'Font Awesome',
                    ),
                ),

                array(
                    'key' => 'social_icon_color',
                    'label' => 'Icon Color',
                    'name' => 'icon_color',
                    'type' => 'color_picker',
                    'wrapper' => array(
                        'width' => '50',
                    ),
                    'instructions' => 'Choose the color for your social media icon',
                    'conditional_logic' => array (
                        array (
                            array (
                                'field' => 'social_iofa',
                                'operator' => '==',
                                'value' => 'Font Awesome',
                            ),
                        ),
                    ),
                ),

                array (
                    'key' => 'social_image',
                    'label' => 'Image',
                    'name' => 'icon',
                    'type' => 'image',
                    'conditional_logic' => array (
                        array (
                            array (
                                'field' => 'social_iofa',
                                'operator' => '==',
                                'value' => 'Image',
                            ),
                        ),
                    ),
                ),
                array (
                    'key' => 'social_fa-icon',
                    'label' => 'FA Icon',
                    'name' => 'fa_icon',
                    'type' => 'font-awesome',
                    'conditional_logic' => array (
                        array (
                            array (
                                'field' => 'social_iofa',
                                'operator' => '==',
                                'value' => 'Font Awesome',
                            ),
                        ),
                    ),
                    'save_format' => 'element',
                    'choices' => array (
                        'null' => '- Select -',
                        'fa-facebook' => ' fa-facebook',
                        'fa-facebook-official' => ' fa-facebook-official',
                        'fa-facebook-square' => ' fa-facebook-square',
                        'fa-google' => ' fa-google',
                        'fa-google-plus' => ' fa-google-plus',
                        'fa-google-plus-official' => ' fa-google-plus-official',
                        'fa-google-plus-square' => ' fa-google-plus-square',
                        'fa-instagram' => ' fa-instagram',
                        'fa-linkedin' => ' fa-linkedin',
                        'fa-linkedin-square' => ' fa-linkedin-square',
                        'fa-plus' => ' fa-plus',
                        'fa-plus-circle' => ' fa-plus-circle',
                        'fa-plus-square' => ' fa-plus-square',
                        'fa-plus-square-o' => ' fa-plus-square-o',
                        'fa-rss' => ' fa-rss',
                        'fa-rss-square' => ' fa-rss-square',
                        'fa-share' => ' fa-share',
                        'fa-share-alt' => ' fa-share-alt',
                        'fa-share-alt-square' => ' fa-share-alt-square',
                        'fa-share-square' => ' fa-share-square',
                        'fa-share-square-o' => ' fa-share-square-o',
                        'fa-twitter' => ' fa-twitter',
                        'fa-twitter-square' => ' fa-twitter-square',
                        'fa-vimeo' => ' fa-vimeo',
                        'fa-vimeo-square' => ' fa-vimeo-square',
                        'fa-youtube' => ' fa-youtube',
                        'fa-youtube-play' => ' fa-youtube-play',
                        'fa-youtube-square' => ' fa-youtube-square',
                    ),
                ),
                
            ),
        ),

	),
	'location' => array(
		array(
			array(
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'general-settings',
			),
		),
	),

));

endif;