<?php
/*------------------------------------*\
	Theme Support
\*------------------------------------*/
if (class_exists('ACF')) {

    // Get the class stucture for the theme
    include 'quse-theme.php';

    // Include the Custom Options for the theme
    include get_template_directory() . '/admin/custom-options.php';

} else {

    echo "I'm sorry, the website functionality is down";

};

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');
}

function custom_admin_css() {
    echo '<style type="text/css">
    .wp-block { max-width: none; }
    </style>';
}
add_action('admin_head', 'custom_admin_css');

// Register Custom Navigation Walker
require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';

// HTML5 Blank navigation
function main_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'collapse navbar-collapse nav--main',
		'container_id'    => 'navbarNav',
		'menu_class'      => 'navbar-nav mr-auto',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul class="navbar-nav">%3$s</ul>',
		'depth'           => 2,
		'walker'          => new WP_Bootstrap_Navwalker()
		)
	);
}

// Register HTML5 Blank Navigation
function register_quse_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'qusedev'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'qusedev'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'qusedev') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// if is plugin active.. add the carousel
/*
?>
<script>
    $(document).ready(function () {
        $('.carousel').find('.carousel-item').first().addClass('active');
    });
</script>
<?php
*/
// Load HTML5 Blank scripts (header.php)
function quse_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        wp_deregister_script('jquery');
	    wp_enqueue_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"), false, '3.3.1');

        wp_register_script('bootstrap', get_template_directory_uri() . '/js/lib/bootstrap.bundle.min.js', array(), '4.0.0'); // Modernizr
        wp_enqueue_script('bootstrap'); // Enqueue it!

        //wp_register_script('ajax-register-script', get_template_directory_uri() . '/js/scripts.js', array(), '1.0.0'); // Custom scripts
        //wp_enqueue_script('ajax-register-script'); // Enqueue it!
        wp_register_script(
            'ajax-register-script', get_template_directory_uri() . '/js/scripts.js', array(), 
        '1.0.0');
        
        
        wp_enqueue_script( 'ajax-register-script' );
        wp_localize_script( 
            'ajax-register-script', 
            'register', 
            array(
                'ajaxurl' => admin_url( 'admin-ajax.php' )
            )
        );

        add_action( 'wp_ajax_nopriv_register', 'ajax__register_user__register' );
        add_action( 'wp_ajax_register', 'ajax__register_user__register' );
        
    }
}

// Load HTML5 Blank styles
function quse_styles()
{

    $today = time();

    wp_register_style('animate', get_template_directory_uri() . '/css/src/animate.css');
    wp_enqueue_style('animate'); // Enqueue it!

    wp_register_style('bootstrap-css', get_template_directory_uri() . '/css/src/bootstrap.min.css');
    wp_enqueue_style('bootstrap-css'); // Enqueue it!

    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!

    wp_register_style('quse', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('quse'); // Enqueue it!

    wp_register_style( 'ga-fontname-style', 'https://fonts.googleapis.com/css?family=Playfair+Display:400,700,700i|Source+Sans+Pro:400,700&display=swap', false, $today, 'all' );
    wp_enqueue_style( 'ga-fontname-style');

    wp_register_style( 'load-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
    wp_enqueue_style( 'load-fa');

}

function quse_admin_styles() {
    wp_register_style('quse-admin', get_template_directory_uri() . '/css/src/admin.css', array(), '1.0', 'all');
    wp_enqueue_style('quse-admin'); // Enqueue it!
}

add_post_type_support( 'page', 'excerpt' );

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

function wpdocs_channel_nav_class( $classes, $item, $args ) {

    if ( 'navbar-nav' === $args->menu_class ) {
        $classes[] = "nav-item";
    }

    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }

    return $classes;
}
add_filter( 'nav_menu_css_class' , 'wpdocs_channel_nav_class' , 10, 4 );

function add_specific_menu_location_atts( $atts, $item, $args ) {
    // check if the item is in the primary menu
    if( 'navbar-nav' === $args->menu_class ) {
      // add the desired attributes:
      $atts['class'] = 'nav-link';
    }
    return $atts;
}
add_filter( 'nav_menu_link_attributes', 'add_specific_menu_location_atts', 10, 3 );

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function qusewp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Remove Admin bar
function remove_admin_bar() { return false; }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/
// Add Actions
add_action('wp_enqueue_scripts', 'quse_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_enqueue_scripts', 'quse_styles'); // Add Theme Stylesheet
add_action('admin_enqueue_scripts', 'quse_admin_styles'); // Add Theme Stylesheet
add_action('init', 'register_quse_menu'); // Add HTML5 Blank Menu
add_action('init', 'qusewp_pagination'); // Add our HTML5 Pagination

// Add Filters
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar

if ( is_admin() ) {

    function remove_dashboard_widgets() {
        global $wp_meta_boxes;
    
        unset($wp_meta_boxes['dashboard']);
    
    }
    add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
    
    remove_action( 'welcome_panel', 'wp_welcome_panel' );
    add_action( 'welcome_panel', 'quse_dashboard_content' );

    // include the admin directory setup file
    include get_template_directory() . '/admin/dashboard.php';
    
}